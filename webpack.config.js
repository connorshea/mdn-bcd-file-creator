const path = require('path');
const devMode = process.env.NODE_ENV !== 'production';
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
  mode: devMode ? 'development' : 'production',
  entry: [ path.resolve(__dirname, 'src/main.js'), path.resolve(__dirname, 'src/main.scss') ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: devMode ? 'bundle.js' : '[hash].bundle.js'
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: devMode ? 'bundle.css' : '[hash].bundle.css'
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: __dirname + '/index.html',
      inject: false
    }),
    new VueLoaderPlugin()
  ],
  devtool: devMode ? "inline-source-map" : "source-map",
  resolve: {
    alias: {
      vue: 'vue/dist/vue.js'
    }
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          { loader: MiniCssExtractPlugin.loader },
          { loader: 'css-loader', options: { sourceMap: true } },
          { loader: 'sass-loader', options: { sourceMap: true } }
        ]
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      }
    ]
  }
};
