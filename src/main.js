import Vue from 'vue';
import App from './App.vue';

window.onload = function() {
  let app = new Vue({
    el: '#app',
    template: '<App/>',
    components: { App }
  });  
}

console.log("test");
