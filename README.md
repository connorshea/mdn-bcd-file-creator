# MDN BCD File Creator

You can get started by running

1. `yarn install`
2. `yarn run watch`

Then open the HTML `index.html` page directly in your browser. Changes will be picked up by Webpack whenever the JavaScript is changed.
